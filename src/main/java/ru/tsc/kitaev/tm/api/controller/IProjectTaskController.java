package ru.tsc.kitaev.tm.api.controller;

public interface IProjectTaskController {

    void bindTaskToProjectById();

    void unbindTaskFromProjectById();

    void removeAllTaskByProjectId();

    void findAllTaskByProjectId();

}
