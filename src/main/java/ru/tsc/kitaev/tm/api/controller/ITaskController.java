package ru.tsc.kitaev.tm.api.controller;

import ru.tsc.kitaev.tm.model.Task;

public interface ITaskController {

    void createTasks();

    void showTasks();

    void showTask(Task task);

    void clearTasks();

    void showById();

    void showByIndex();

    void showByName();

    void removeById();

    void removeByName();

    void removeByIndex();

    void updateByIndex();

    void updateById();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();

    void changeStatusById();

    void changeStatusByIndex();

    void changeStatusByName();

}
